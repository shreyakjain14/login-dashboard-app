import { Component } from '@angular/core';
import { AuthService } from 'src/app/providers/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'dashboard.html',
  styleUrls: ['dashboard.scss'],
})
export class DashboardPage {
  constructor(private authService: AuthService) {}

  search(event) {}

  logout() {
    this.authService.logout();
  }
}
