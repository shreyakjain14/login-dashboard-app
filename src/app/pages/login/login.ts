import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/providers/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.html',
  styleUrls: ['login.scss'],
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;
  public isInvalidCredentials: boolean;
  public serverError: any;

  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: AuthService
  ) {}

  public ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
    });
  }

  login() {
    if (this.loginForm.invalid) {
      console.log(this.loginForm);
      return;
    }

    this.http
      .post('http://52.5.173.237/login.php', this.loginForm.value)
      .subscribe(
        (response: any) => {
          console.log('response is::: ', response);
          if (!response.status) {
            this.isInvalidCredentials = true;
          } else {
            this.authService.login();
            this.router.navigate(['dashboard']);
          }
        },
        (error) => {
          console.log('error is:: ', error);
          this.serverError = error;
        }
      );
  }
}
